import '../css/Footer.css'

function Privacidad() {
    return (
        <>

            {/* <!-- Button trigger modal --> */}
            <li data-toggle="modal" data-target="#Privacidad">&middot; Privacidad&nbsp;</li>

            {/* <!-- Modal --> */}
            <div className="modal fade text-dark" data-backdrop="static" id="Privacidad" tabIndex="-1" role="dialog" aria-labelledby="examplePrivacidad" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title" id="exampleModalLongTitle">Política de privacidad</h4>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div className="modal-body">
                            <h5>Protección de datos</h5>
                            <p>Se le garantiza al usuario la no divulgación de sus datos bajo las leyes que lo protegen.</p>

                            <h5>Propiedad intelectual e industrial</h5>
                            <p>Se protegen todos los derechos de autor que el sitio genere, como pueden ser contenido, audios, videos, logotipos, etc.</p>

                            <h5>Derecho de exclusión</h5>
                            <p>El sitio web se reserva el derecho de restringir el acceso a cualquier usuario registrado.</p>
                        </div>

                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary btn-block" data-dismiss="modal">Acepto</button>
                        </div>

                    </div>
                </div>
            </div>

        </>
    );
}

function Terminos() {
    return (
    <>

        {/* <!-- Button trigger modal --> */}
        <li data-toggle="modal" data-target="#Terminos">&middot; Terminos&nbsp;</li>

        {/* <!-- Modal --> */}
        <div className="modal fade text-dark" data-backdrop="static" id="Terminos" tabIndex="-1" role="dialog" aria-labelledby="exampleTerminos" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h4 className="modal-title" id="exampleModalLongTitle">Términos de uso</h4>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div className="modal-body">
                        <h5>Aviso legal y terminos de uso</h5>
                        <p>Al utilizar nuestro Sitio Web se da por entendido que acepta nuestro Aviso legal y términos de uso, Políticas de privacidad y Póliticas de cookies.</p>

                        <h5>Exclusión de garantías y responsabilidades</h5>
                        <p>El sitio web se deslinda de todos los daños y perjuicios ocasionados por el mismo, por ejemplo, la no disponibilidad del sitio.</p>


                        <h5>Modificaciones</h5>
                        <p>El sitio se reserva el derecho a realizar las modificaciones necesarias, incluso si sus funcionalidades se vuelven no vigentes.</p>


                    </div>

                    <div className="modal-footer">
                        <button type="button" className="btn btn-primary btn-block" data-dismiss="modal">Acepto</button>
                    </div>

                </div>
            </div>
        </div>

    </>);
}

function Cookies() {
    return (
    <>

        {/* <!-- Button trigger modal --> */}
        <li data-toggle="modal" data-target="#Cookies">&middot; Cookies&nbsp;</li>

        {/* <!-- Modal --> */}
        <div className="modal fade text-dark" data-backdrop="static" id="Cookies" tabIndex="-1" role="dialog" aria-labelledby="Cookies" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h4 className="modal-title" id="exampleModalLongTitle">Política de cookies</h4>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div className="modal-body">
                        <h5>No utilizamos cookies.</h5>
                    </div>

                    <div className="modal-footer">
                        <button type="button" className="btn btn-primary btn-block" data-dismiss="modal">Acepto</button>
                    </div>

                </div>
            </div>
        </div>

    </>
    );
}

function Footer() {
        return (
            <div className="Footer p-3 mx-auto bg-primary text-white">
                <ul className="nav">
                    <li>WebGC &copy;{(new Date()).getFullYear()}&nbsp;</li>
                    <Privacidad />
                    <Terminos />
                    <Cookies />
                </ul>
            </div>
        );
}

export default Footer;