function Home() {
    return (
        <div className="container p-3 bg-white colorBlack">

            <h1>—Maestro, ¿cuál es el gran mandamiento de la ley? <p>Mateo 22:36-40</p></h1>

            <p>Jesús le dijo: —Amarás al Señor tu Dios con todo tu corazón y con toda tu alma y con toda tu mente.</p>

            <p>Este es el grande y el primer mandamiento.</p>

            <p>Y el segundo es semejante a él: Amarás a tu prójimo como a ti mismo.</p>

            <p>De estos dos mandamientos dependen toda la Ley y los Profetas.</p>

            <h1>Si me amáis, guardaréis mis mandamientos.<p>Juan 14:15</p></h1>

            <p>1. "No tendrás otros dioses.</p>

            <p>2. "No te harás imágenes, no te inclinarás ante ellas ni les rendirás culto.</p>

            <p>3. "No tomarás en vano el nombre de Dios.</p>

            <p>4. "Acuérdate del día <strong>sábado</strong> para santificarlo.</p>

            <p>5. "Honra a tu padre y a tu madre.</p>

            <p>6. "No cometerás homicidio.</p>

            <p>7. "No cometerás adulterio.</p>

            <p>8. "No robarás.</p>

            <p>9. "No darás falso testimonio contra tu prójimo.</p>

            <p>10. "No codiciarás nada.</p>

            <h1>Éxodo 20:3-17 / Deuteronomio 5:7-21</h1>

        </div>
    );
}

export default Home;