import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './css/index.css';
/* import reportWebVitals from './config/reportWebVitals'; */

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
/* reportWebVitals(console.log); */